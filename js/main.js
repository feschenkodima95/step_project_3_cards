import { routes, getToken, getCards } from "./authorization.js";
import {
  Visit,
  VisitCardiologist,
  VisitDentist,
  VisitTherapist,
} from "./visit_classes.js";
import {
  Modal,
  CardiologistModal,
  DentistModal,
  TherapistModal,
  form,
} from "./modal_classes.js";
const token = sessionStorage.getItem("token");
const loginButton = document.getElementById("js-login");
const createVisitButton = document.getElementById("js-create");
const cardsContainer = document.getElementById("cards-container");

let cardListData = [];
const filterForm = document.getElementById("navbar-filter");
filterForm.style.display = "none";

function deleteCard(id) {
  const getRoute = routes.get("CARD");
  const route = getRoute(id);
  const config = {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${sessionStorage.getItem("token")}`,
    },
  };
  return fetch(route, config);
}

let modal = new Modal();
function renderCards(cardListData) {
  cardListData.forEach((item, index) => {
    if (item.title === "cardiologist") {
      const cardiologistCard = new VisitCardiologist();
      cardsContainer.append(
        cardiologistCard.renderCardiologistCard(
          item.title,
          item.target,
          item.description,
          item.urgency,
          item.name,
          item.id,
          item.pressure,
          item.weightIndex,
          item.diseases,
          item.age
        )
      );
    } else {
      if (item.title === "dentist") {
        const dentistCard = new VisitDentist();
        cardsContainer.append(
          dentistCard.renderDentistCard(
            item.title,
            item.target,
            item.description,
            item.urgency,
            item.name,
            item.id,
            item.lastVisit
          )
        );
      } else {
        if (item.title === "therapist") {
          const therapistCard = new VisitTherapist();
          cardsContainer.append(
            therapistCard.renderTherapistCard(
              item.title,
              item.target,
              item.description,
              item.urgency,
              item.name,
              item.id,
              item.age
            )
          );
        }
      }
    }

    const currentCard = document.querySelectorAll("#edit")[index];
    const currentCardForDelete = document.querySelectorAll(".del")[index];

    currentCardForDelete.addEventListener("click", async (e) => {
      e.preventDefault();
      const currentId = e.currentTarget.dataset.cardId;

      const delCard = await deleteCard(currentId);

      document.querySelector(".card_discription").remove();
    });
    currentCard.addEventListener("click", (event) => {
      event.preventDefault();

      const currentId = event.currentTarget.dataset.cardId;
      const currentTargetCard = cardListData.filter(
        ({ id }) => id == currentId
      );
      // let modal = new Modal();
      modal.renderForm(currentTargetCard[0], true);
      form.style.display = "flex";
      let closeModalButton = document.getElementById("close-button");
      closeModalButton.addEventListener("click", (e) => {
        e.preventDefault();

        form.style.display = "none";
      });
    });
  });
}

document.addEventListener("DOMContentLoaded", async function () {
  if (!!token) {
    loginButton.style.display = "none";
    createVisitButton.style.display = "block";
    const cardsResp = await getCards();
    cardListData = await cardsResp.json();
    filterForm.style.display = "flex";
    renderCards(cardListData);
  } else {
    loginButton.addEventListener("click", (e) => {
      const emailInput = document.getElementById("exampleInputEmail1");
      const passwordInput = document.getElementById("exampleInputPassword1");
      e.preventDefault();
      document.querySelector(".authorization-form").style.display = "block";
      document
        .getElementById("confirm-auth")
        .addEventListener("click", async (e) => {
          e.preventDefault();
          const resp = await getToken(emailInput.value, passwordInput.value);

          if (resp.ok) {
            const token = await resp.text();
            sessionStorage.setItem("token", token);
            const cardsResp = await getCards();
            cardListData = await cardsResp.json();
            renderCards(cardListData);
            loginButton.style.display = "none";
            document.querySelector(".authorization-form").style.display =
              "none";
            createVisitButton.style.display = "block";
            filterForm.style.display = "flex";
          } else {
            emailInput.value = "";
            passwordInput.value = "";
            let errorMessage = document.querySelector(".error-message");
            errorMessage.style.display = "block";
          }
        });
    });
  }
  createVisitButton.addEventListener("click", (e) => {
    e.preventDefault();
    modal.renderForm();

    let closeModalButton = document.getElementById("close-button");
    closeModalButton.addEventListener("click", (e) => {
      e.preventDefault();

      form.style.display = "none";
    });
  });
});

const cardiologist = new CardiologistModal();


const BASE_URL = "https://ajax.test-danit.com/api/v2";
const routes = new Map([
  ["LOGIN", () => `${BASE_URL}/cards/login`],
  ["CARDS", () => `${BASE_URL}/cards`],
  ["CARD", (id) => `${BASE_URL}/cards/${id}`],
]);

export function getToken(email, password) {
  const getRoute = routes.get("LOGIN");
  const route = getRoute();
  const config = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email, password }),
  };
  return fetch(route, config);
}

export function getCards() {
  const getRoute = routes.get("CARDS");
  const route = getRoute();
  const config = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${sessionStorage.getItem("token")}`,
    },
  };
  return fetch(route, config);
}
export { BASE_URL, routes };

import { getCards, routes } from "./authorization.js";

const form = document.getElementById("js-form");
function createCard(body) {
  const getRoute = routes.get("CARDS");
  const route = getRoute();
  const config = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${sessionStorage.getItem("token")}`,
    },
    body,
  };
  return fetch(route, config);
}

function updateCard(id, body) {
  const getRoute = routes.get("CARD");
  const route = getRoute(id);
  const config = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${sessionStorage.getItem("token")}`,
    },
    body,
  };
  return fetch(route, config);
}

export class Modal {
  constructor(title, target, description, urgency, name) {
    this.title = title;
    this.target = target;
    this.description = description;
    this.urgency = urgency;
    this.name = name;
  }
  renderForm(visitData = {}, isUpdate) {
    const {
      title = "cardiologist",
      target = "",
      description = "",
      urgency = "",
      name = "",
      pressure = "",
      weightIndex = "",
      diseases = "",
      age = "",
      lastVisit = "",
      id,
    } = visitData;

    form.innerHTML = `
      <select value="${title}" name="title" id="title">
        <!-- <option value="select doctor">Select doctor</option> -->
        <option value="cardiologist">cardiologist</option>
        <option value="dentist">dentist</option>
        <option value="therapist">therapist</option>
      </select>
      <div class="input-group input-group-sm mb-3">
        <span class="input-group-text" id="inputGroup-sizing-sm"
          >Цель визита</span
        >
        <input
          id="target"
          name="target"
          value="${target}"
          type="text"
          class="form-control"
          aria-label="Sizing example input"
          aria-describedby="inputGroup-sizing-sm"
        />
      </div>
    
      <div class="input-group input-group-sm mb-3">
        <span class="input-group-text" id="inputGroup-sizing-sm"
          >Описание визита</span
        >
        <input
          id="description"
          name="description"
          type="text"
          value="${description}"
          class="form-control"
          aria-label="Sizing example input"
          aria-describedby="inputGroup-sizing-sm"
        />
      </div>
    
      <select
        id="urgency"
        name="urgency"
        class="form-select form-select-sm"
        value="${urgency}"
        aria-label=".form-select-sm example"
      >
      <option value="regular">regular</option>
      <option value="priority">priority</option>
      <option value="urgent">urgent</option>
      </select>
    
      <div class="input-group input-group-sm mb-3" id="modal-window">
        <span class="input-group-text" id="inputGroup-sizing-sm"
          >Ваше имя</span
        >
        <input
          id="name"
          name="name"
          type="text"
          value="${name}"
          class="form-control"
          aria-label="Sizing example input"
          aria-describedby="inputGroup-sizing-sm"
        />
      </div>
      <div class="inputs-container"></div>
      <div class="modal-buttons">
            <button type="button" class="btn btn-success" id="create-card-btn">
              ${isUpdate ? "Обновить" : "Создать"}
            </button>
            <button type="button" class="btn btn-danger" id="close-button">
              Закрыть
            </button>
          </div>
    `;

    let cardiologistModal = new CardiologistModal();
    cardiologistModal.renderCardiologistModal();
    // const form = document.getElementById("js-form");
    form.style.display = "flex";
    const selectDoctor = document.getElementById("title");
    selectDoctor.addEventListener("change", function (e) {
      //SWITСH ДЛЯ ФОРМ СОЗДАНИЯ КАРТОЧКИ
      const title = e.target.value;

      switch (title) {
        case "cardiologist":
          let cardiologistModal = new CardiologistModal();
          cardiologistModal.renderCardiologistModal(visitData);
          break;
        case "dentist":
          let dentistModal = new DentistModal();
          dentistModal.renderDentistModal(visitData);
          break;
        case "therapist":
          let therapistModal = new TherapistModal();
          therapistModal.renderTherapistModal(visitData);
          break;
      }
    });
    selectDoctor.value = title;
    switch (title) {
      case "cardiologist":
        let cardiologistModal = new CardiologistModal();
        cardiologistModal.renderCardiologistModal(visitData);
        break;
      case "dentist":
        let dentistModal = new DentistModal();
        dentistModal.renderDentistModal(visitData);

        break;
      case "therapist":
        let therapistModal = new TherapistModal();
        therapistModal.renderTherapistModal(visitData);
        break;
    }
    document
      .getElementById("create-card-btn")
      .addEventListener("click", async (e) => {
        e.preventDefault();
        const formData = new FormData(form);

        const object = {};
        formData.forEach(function (value, key) {
          object[key] = value;
        });
        if (isUpdate) {
          updateCard(id, JSON.stringify(object));
        } else {
          createCard(JSON.stringify(object));
        }
        form.style.display = "none";
      });
  }
}

export class CardiologistModal extends Modal {
  constructor(
    title,
    target,
    description,
    urgency,
    name,
    pressure,
    weightIndex,
    diseases,
    age
  ) {
    super(title, target, description, urgency, name);
    this.pressure = pressure;
    this.weightIndex = weightIndex;
    this.diseases = diseases;
    this.age = age;
  }
  renderCardiologistModal({
    pressure = "",
    weightIndex = "",
    diseases = "",
    age = "",
  } = {}) {
    const inputsContainer = document.querySelector(
      "#js-form .inputs-container"
    );
    inputsContainer.innerHTML = `
    <div class="input-group input-group-sm mb-3">
      <span class="input-group-text" id="inputGroup-sizing-sm">Давление</span>
      <input id='pressure' name='pressure' type="number" class="form-control" aria-label="Sizing example input" value="${pressure}" aria-describedby="inputGroup-sizing-sm">
    </div>
    
    <div class="input-group input-group-sm mb-3">
      <span class="input-group-text" id="inputGroup-sizing-sm">Индекс массы тела</span>
      <input id='weightIndex' name='weightIndex' type="number" value='${weightIndex}' class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
    </div>
    
    <div class="input-group input-group-sm mb-3">
      <span class="input-group-text" id="inputGroup-sizing-sm">Хронические заболевания</span>
      <input id='diseases' name='diseases' type="text" class="form-control" value='${diseases}' aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
    </div>
    
    <div class="input-group input-group-sm mb-3">
      <span class="input-group-text" id="inputGroup-sizing-sm">Возраст</span>
      <input id='age' name='age' type="number" class="form-control" value='${age}' aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
    </div>
    `;
  }
}

export class DentistModal extends Modal {
  constructor(title, target, description, urgency, name, lastVisit) {
    super(title, target, description, urgency, name);
    this.lastVisit = lastVisit;
  }
  renderDentistModal({ lastVisit = "" } = {}) {
    const inputsContainer = document.querySelector(
      "#js-form .inputs-container"
    );
    inputsContainer.innerHTML = `  
    <div class="input-group input-group-sm mb-3">
      <span class="input-group-text" id="inputGroup-sizing-sm">Последний визит</span>
      <input id='lastVisit' name='lastVisit' type="date" value='${lastVisit}' class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
    </div>
    `;
  }
}

export class TherapistModal extends Modal {
  constructor(title, target, description, urgency, name, age) {
    super(title, target, description, urgency, name);
    this.age = age;
  }
  renderTherapistModal({ age = "" } = {}) {
    const inputsContainer = document.querySelector(
      "#js-form .inputs-container"
    );
    inputsContainer.innerHTML = `
    <div class="input-group input-group-sm mb-3">
      <span class="input-group-text" id="inputGroup-sizing-sm">Возраст</span>
      <input id='age' name='age' type="number" class="form-control" value='${age}' aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
    </div>
    `;
  }
}

export { form };

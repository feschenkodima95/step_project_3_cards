export class Visit {
  constructor(title, target, description, urgency, name, id) {
    this.title = title;
    this.target = target;
    this.description = description;
    this.urgency = urgency;
    this.name = name;
    this.id = id;
  }
  showMore() {
    const showMoreButton = document.querySelector(".more");
    // const card = document.querySelector(".card_discription");
    const visitDiscription = document.querySelector(".show-more");
    cardsContainer.addEventListener("click", (e) => {
      e.preventDefault();
      if (e.target == showMoreButton)
        visitDiscription.style.display =
          visitDiscription.style.display == "block" ? "none" : "block";
      console.log(e.target);
    });
  }
}
export class VisitCardiologist extends Visit {
  constructor(
    title,
    target,
    description,
    urgency,
    name,
    id,
    pressure,
    weightIndex,
    diseases,
    age
  ) {
    super(title, target, description, urgency, name, id);
    this.pressure = pressure;
    this.weightIndex = weightIndex;
    this.diseases = diseases;
    this.age = age;
  }
  renderCardiologistCard(
    title,
    target,
    description,
    urgency,
    name,
    id,
    pressure,
    weightIndex,
    diseases,
    age
  ) {
    let cardItem = document.createElement("div");

    cardItem.innerHTML = `<div class="card_discription ${urgency} ${title}">
    <h2>${title}</h2>
    <h3>${name}</h3>
    <p>
      <a
        class="nav-item"
        data-bs-toggle="collapse"
        href="#collapseExample${id}"
        role="button"
        aria-expanded="false"
        aria-controls="collapseExample"
      >
        Show more
      </a>
    </p>
    <ul class="show-more collapse" id="collapseExample${id}">
        <li>Target of visit: ${target}</li>
        <li>Description: ${description}</li>
           <li>Priority: ${urgency} </li>
        <li>Blood pressure: ${pressure}</li>
        <li>ИWeight index: ${weightIndex}</li>
        <li>past diseases of the cardiovascular system: ${diseases}</li>
        <li>возраст: ${age} </li>
    </ul>
    <div>
      <button id='edit' class="card_create_btn button"  data-card-id="${id}">Edit card</button>
      <button class="card_create_btn del button" data-card-id="${id}">Delete card</button>
    </div>
    </div>`;
    return cardItem;
  }
}

export class VisitDentist extends Visit {
  constructor(title, target, description, urgency, name, id, lastVisit) {
    super(title, target, description, urgency, name, id);
    this.lastVisit = lastVisit;
  }
  renderDentistCard(title, target, description, urgency, name, id, lastVisit) {
    let cardItem = document.createElement("div");

    cardItem.innerHTML = `<div class="card_discription ${urgency} ${title}">
    <h2>${title}</h2>
    <h3>${name}</h3>
    <p>
      <a
        class="nav-item"
        data-bs-toggle="collapse"
        href="#collapseExample${id}"
        role="button"
        aria-expanded="false"
        aria-controls="collapseExample"
      >
        Show more
      </a>
    </p>
    <ul class="show-more collapse" id="collapseExample${id}">
    <li>Target of visit: ${target}</li>
    <li>Description: ${description}</li>
       <li>Priority: ${urgency} </li>
        <li>Last visit: ${lastVisit} </li>
    </ul>
    <div>
      <button id='edit' class="card_create_btn button"   data-card-id="${id}">Edit card</button>
      <button class="card_create_btn del button" data-card-id="${id}">Delete card</button>
    </div>
    </div>`;
    return cardItem;
  }
}

export class VisitTherapist extends Visit {
  constructor(title, target, description, urgency, name, id, age) {
    super(title, target, description, urgency, name, id);
    this.age = age;
  }
  renderTherapistCard(title, target, description, urgency, name, id, age) {
    let cardItem = document.createElement("div");

    cardItem.innerHTML = `<div class="card_discription ${urgency} ${title} ">
    <h2>${title}</h2>
    <h3>${name}</h3>
    <p>
      <a
        class="nav-item"
        data-bs-toggle="collapse"
        href="#collapseExample${id}"
        role="button"
        aria-expanded="false"
        aria-controls="collapseExample"
      >
        Show more
      </a>
    </p>
    
    <ul class="show-more collapse" id="collapseExample${id}">
    <li>Target of visit: ${target}</li>
    <li>Description: ${description}</li>
       <li>Priority: ${urgency} </li>
        <li>Age: ${age} </li>
    </ul>
    <div>
      <button id='edit' class="card_create_btn button"   data-card-id="${id}">Редактировать</button>
      <button class="card_create_btn del button" data-card-id="${id}">Удалить карту</button>
    </div>
    </div>`;
    return cardItem;
  }
}
